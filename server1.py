import cv2
import socket
import time
from threading import Thread
import base64
import sys

FPS = 30

server_add = socket.gethostbyname(socket.gethostname())

server_add = "192.168.1.34"

print(server_add)



streaming_port = 8081
com_port = 8082

list_of_active_clients = []
list_of_active_streaming_clients = []

is_streaming = False

camera = cv2.VideoCapture(0, cv2.CAP_DSHOW)
camera.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

def check_client(client_socket):
    pass#to be added -> signal client once in a while to check if connection is active -> if not close connection

def stream_handling_thread(client_str_socket, client_str_add):
    print(f"[STREAM THREAD STARTED] {client_str_add}")
    global is_streaming

    while is_streaming:
        try:
            data = camera.read()[1]
            retval, packet = cv2.imencode('.jpg', data)
            packet = bytes(packet)
            packet = base64.b64encode(packet)
            client_str_socket.sendall(packet)
            time.sleep(1/FPS)
        except:
            print("[STREAM CRASHED]")
            break

    client_str_socket.close()
    print(f"[STREAM ENDED] {client_str_add}")
    list_of_active_streaming_clients.remove(client_str_add)
     




def client_handling_thread(client_socket, client_add):
    print(f"[NEW THREAD STARTED FOR {client_add}]")
    global is_streaming
    while True:
        try:
            packet = client_socket.recv(1024).decode()

            if(packet == "start_stream" and not is_streaming):
                client_socket.sendall("str_soc_can_acc".encode())#com socket should send a key to client, after establishing connection with Str_sock client should send back key to validate com.
                waiting = True
                while(waiting):
                    client_str_socket, client_str_add = stream_socket.accept()
                    if(client_str_add):
                        waiting = False

                stream_thread = Thread(target=stream_handling_thread, args=(client_str_socket, client_str_add))   
                is_streaming = True
                stream_thread.start()
                list_of_active_streaming_clients.append(client_str_add)

            elif(packet == "start_stream" and is_streaming):
               client_socket.sendall("already_streaming".encode())

            elif(packet == "end_stream"):
               is_streaming = False
               pass

            elif(packet == "end_com"):
                print(f"ENDING CONNECTION FOR {client_add}")
                is_streaming = False
                time.sleep(0.5)
                break

        except:
           print("UNEXPECTED ERROR - COMMUNICATION CRASHED")
           is_streaming = False
           break
    print(f"[CLOSING THREAD] {client_add}")
    list_of_active_clients.remove(client_add)
    client_socket.close()
    
com_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print(f"1. [COM SOCKET STARTED]")

com_socket.bind((server_add, com_port))
print(f"2. [COM SOCKET BINDED] {(server_add, com_port)}")

stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("3. [STR SOCKET STARTED]")

stream_socket.bind((server_add, streaming_port))
print(f"4. [STR SOCKET BINDED] {(server_add, streaming_port)}")

com_socket.listen()
print("5. [COM SOCKET LISTENING...]")

stream_socket.listen()
print("6. [STR SOCKET LISTENING...]")

while True:
    try:
        if(len(list_of_active_clients) < 1):
            client_socket, client_add = com_socket.accept()

            client_thread = Thread(target=client_handling_thread, args=(client_socket, client_add))
            client_thread.start()

            list_of_active_clients.append(client_add)
            print(f"[ACTIVE CLIENTS] {len(list_of_active_clients)}")
        else:
            pass
    except KeyboardInterrupt:
        break


com_socket.close()