import socket
import cv2
from threading import Thread
import time
import base64
import np
import sys

def validate_ip(string):
    arr = string.split(".")
    if len(arr) != 4:
        return False
    else:
        for x in arr:
            if not x.isdigit():
                return False
            i = int(x)
            if i < 0 or i > 255:
                return False
    return True

def validate_ip_local(string):
    arr = string.split(".")
    if len(arr) != 4:
        return False
    else:
        if int(arr[0]) == 192 and int(arr[1]) == 168 and int(arr[3]) == 1:
            return True
        else:
            print("This might not be local IP")
            sys.exit(1)

is_streaming = False

if(len(sys.argv) == 1):
    entry = input("Enter server IP:")
    if(not validate_ip(entry)):
        print("Provided string is not IP address, trying default IP")
        server_add = "192.168.1.15"#default addres
    else:
        server_add = entry
else:
    if(validate_ip(sys.argv[-1])):
        server_add = sys.argv[-1]
        print("Setting provided IP")
    else:
        print("Provided argument is not IP address, setting default IP")
        server_add = "192.168.1.15"#default addres

com_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    com_socket.connect((server_add, 8082))
except:
    print("Failed to connect to server. Check if server is running/server_add is ok")
    sys.exit(1)

def decode_image(source):
    jpg_in_bytes = base64.b64decode(source)
    image_np = np.frombuffer(jpg_in_bytes, np.uint8)
    img_np = cv2.imdecode(image_np, cv2.IMREAD_COLOR)  
    return img_np

def handle_stream(stream_socket):
    global is_streaming
    while is_streaming:
        try:
            img = stream_socket.recv(131072)
            if img:
                new = decode_image(img)
                cv2.imshow("Frame", new)
                cv2.waitKey(1)
                stream_socket.sendall("ack".encode())
        except:
            pass
    cv2.destroyAllWindows()


while True:
    data = input("Enter command: ")
    com_socket.sendall(data.encode())

    if(data=="start_stream"):
        if(is_streaming==False):
            is_streaming = True
            ack_recived = False
            while(not ack_recived):
                packet = com_socket.recv(1024).decode()
                if(packet == "str_soc_can_acc"):
                    ack_recived = True
                    stream_socket.connect((server_add, 8081))
                    stream_thread = Thread(target=handle_stream, args=(stream_socket,))
                    stream_thread.start()
            
        else:
            print("[ALREADY STREAMING]")
    if(data=="end_stream"):
        is_streaming = False

    if(data=="end_com"):
        break